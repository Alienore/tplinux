🌞 Installer MariaDB sur la machine db.tp5.linux

[alienor@db ~]$ sudo dnf install mariadb-server
[sudo] password for alienor:
Rocky Linux 8 - AppStream                            10 kB/s | 4.8 kB     00:00
Rocky Linux 8 - BaseOS                              8.0 kB/s | 4.3 kB     00:00
Rocky Linux 8 - Extras                              6.7 kB/s | 3.5 kB     00:00
Rocky Linux 8 - Extras                               19 kB/s |  10 kB     00:00
Dependencies resolved.
====================================================================================
 Package             Arch   


🌞 Le service MariaDB

[alienor@db ~]$ systemctl start
Too few arguments.
[alienor@db ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.
[alienor@db ~]$ ^C


[alienor@db ~]$ systemctl start mariadb
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ====
Authentication is required to start 'mariadb.service'.
Authenticating as: alienor
Password:
==== AUTHENTICATION COMPLETE ====


[alienor@db ~]$ sudo ss -mariadb
[sudo] password for alienor:
State   Recv-Q    Send-Q       Local Address:Port       Peer Address:Port   Process
[alienor@db ~]$



[alienor@db ~]$ ps -ef | grep mariadb
alienor     6070    1488  0 14:28 pts/0    00:00:00 grep --color=auto mariadb
[alienor@db ~]$




🌞 Firewall

[alienor@db ~]$ sudo firewall-cmd --zone=public --add-port=80/tcp
success




🌞 Configuration élémentaire de la base


Remove anonymous users? [Y/n] y
 ... Success
-verifier les utilisateurs, eviter les malveillants
Disallow root login remotely? [Y/n] y
 ... Success!
-interdir la connexion root a distance, eviter qu'on s'incruste sur mon reseau 
Remove test database and access to it? [Y/n] y
 - Dropping test database...
 ... Success!
 - supprimer des donnes pour supprimer aussi les utilisateurs potentiellement malaveillant
- eviter que tous est acces 
 - Removing privileges on test database...
 ... Success!
-eviter que tout le monde y accéde a la base de donnée
Reload privilege tables now? [Y/n] y
 ... Success!
- 




🌞 Préparation de la base en vue de l'utilisation par NextCloud
 
[alienor@db ~]$ sudo mysql -u root -p
[sudo] password for alienor:
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 28
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]>
MariaDB [(none)]>
MariaDB [(none)]> # Création d'un utilisateur dans la base, avec un mot de passe
MariaDB [(none)]> # L'adresse IP correspond à l'adresse IP depuis laquelle viendra les connexions. Cela permet de restreindre les IPs autorisées à se connecter.
MariaDB [(none)]> # Dans notre cas, c'est l'IP de web.tp5.linux
MariaDB [(none)]> # "meow" c'est le mot de passe :D
MariaDB [(none)]> CREATE USER 'nextcloud'@'10.5.1.11' IDENTIFIED BY 'meow';
Query OK, 0 rows affected (0.001 sec)

MariaDB [(none)]>
MariaDB [(none)]> # Création de la base de donnée qui sera utilisée par NextCloud
MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.001 sec)

MariaDB [(none)]>
MariaDB [(none)]> # On donne tous les droits à l'utilisateur nextcloud sur toutes les tables de la base qu'on vient de créer
MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.5.1.11';
Query OK, 0 rows affected (0.001 sec)

MariaDB [(none)]>
MariaDB [(none)]> # Actualisation des privilèges
MariaDB [(none)]> FLUSH PRIVILEGES;





[alienor@db ~]$ sudo dnf provides mysql
Last metadata expiration check: 1:15:46 ago on Thu 02 Dec 2021 02:01:15 PM CET.
mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64 : MySQL client programs and shared
                                                  : libraries
Repo        : appstream
Matched from:
Provide    : mysql = 8.0.26-1.module+el8.4.0+652+6de068a7




🌞 Installez sur la machine web.tp5.linux la commande mysql


[alienor@web ~]$ sudo dnf install mysql
Rocky Linux 8 - AppStream                            12 kB/s | 4.8 kB     00:00
Rocky Linux 8 - BaseOS                              9.9 kB/s | 4.3 kB     00:00
Rocky Linux 8 - BaseOS                              2.0 MB/s | 3.5 MB     00:01
Rocky Linux 8 - Extras                              8.0 kB/s | 3.5 kB     00:00
Rocky Linux 8 - Extras                               16 kB/s |  10 kB     00:00
Dependencies resolved.
====================================================================================
 Package                Arch   Version                              Repo       Size
====================================================================================
Installing:
 mysql                  x86_64 8.0.26-1.module+el8.4.0+652+6de068a7 appstream  12 M
Installing dependencies:
 mariadb-connector-c-config
                        noarch 3.1.11-2.el8_3                       appstream  14 k
 mysql-common           x86_64 8.0.26-1.module+el8.4.0+652+6de068a7 appstream 133 k
Enabling module stream







🌞 Tester la connexion

[alienor@web ~]$ mysql -h 10.5.1.11 -P 3306 -u nextclood -p
Enter password:
ERROR 2003 (HY000): Can't connect to MySQL server on '10.5.1.11:3306' (111)






























A. Apache
🌞 Installer Apache sur la machine web.tp5.linux


[alienor@node1 ~]$ sudo dnf install httpd
Rocky Linux 8 - AppStrea  13 kB/s | 4.8 kB     00:00
Rocky Linux 8 - BaseOS    14 kB/s | 4.3 kB     00:00
Rocky Linux 8 - Extras    12 kB/s | 3.5 kB     00:00
Dependencies resolved.
=========================================================
 Package           Arch   Version        Repo       Size
=========================================================
Installing:
 httpd             x86_64 2.4.37-43.module+el8.5.0+714+5e









🌞 Analyse du service Apache


[alienor@web ~]$ systemctl start httpd
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ====
Authentication is required to start 'httpd.service'.
Authenticating as: alienor
Password:
==== AUTHENTICATION COMPLETE ====


[alienor@node1 ~]$ sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
[alienor@node1 ~]$
[alienor@node1 ~]$ systemclt status httpd
-bash: systemclt: command not found
[alienor@node1 ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service>
   Active: active (running) since Sun 2021-11-28 22:16:1>
     Docs: man:httpd.service(8)
 Main PID: 2589 (httpd)






[alienor@node1 ~]$ ps -ef | grep httpd
root        2589       1  0 22:16 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2590    2589  0 22:16 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2591    2589  0 22:16 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2592    2589  0 22:16 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2595    2589  0 22:16 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
alienor     2836    1644  0 22:27 pts/1    00:00:00 grep --color=auto httpd



[alienor@web ~]$ sudo ss -ltnp | grep httpd
LISTEN 0      128                *:80              *:*    users:(("httpd",pid=895,fd=4),("httpd",pid=894,fd=4),("httpd",pid=893,fd=4),("httpd",pid=869,fd=4))
[alienor@web ~]$



🌞 Un premier test


[alienor@node1 ~]$ sudo firewall-cmd --zone=public --add-port=80/tcp
[sudo] password for alienor:
success


alien@LAPTOP-F1TNBS44 MINGW64 ~
$ curl -x  exempledeproxy.com:8090 -U nomutilisateur:motdepasse -O http://
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0curl: (5) Could not resolve proxy: exempledeproxy.com




B. PHP

🌞 Installer PHP

[alienor@web ~]$ sudo dnf install epel-release
[sudo] password for alienor:
Last metadata expiration check: 0:16:37 ago on Thu 02 Dec 2021 03:20:46 PM CET.
Dependencies resolved.
==============

[alienor@web ~]$ sudo dnf update
Extra Packages for Enterprise Linux Modular 8 - x86 746 kB/s | 958 kB     00:01
Last metadata expiration check: 0:00:02 ago on Thu 02 Dec 2021 03:39:42 PM CET.
Dependencies resolved.
Nothing to do.
Complete!



[alienor@web ~]$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
Last metadata expiration check: 0:00:46 ago on Thu 02 Dec 2021 03:39:42 PM CET.
remi-release-8.rpm                                  155 kB/s |  26 kB     00:00
Dependencies resolved.
======================



alienor@web ~]$ sudo dnf module enable php:remi-7.4
Last metadata expiration check: 0:00:49 ago on Thu 02 Dec 2021 03:42:26 PM CET.
Dependencies resolved.
Nothing to do.
Complete!
[alienor@web ~]$


[alienor@web ~]$ sudo dnf install zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
Last metadata expiration check: 0:01:30 ago on Thu 02 Dec 2021 03:42:26 PM CET.
Package zip-3.0-23.el8.x86_64 is already installed.
Package unzip-6.0-45.el8_4.x86_64 is already installed.
Package libxml2-2.9.7-11.el8.x86_64 is already installed.
Package openssl-1:1.1.1k-4.el8.x86_64 is already installed.
Dependencies resolved.
====================================================================================
 Package                  Arch        Version                  Repository      Size
=========================================




🌞 Analyser la conf Apache

[alienor@web ~]$ cd /etc/httpd/conf/
[alienor@web conf]$ cd httpd.conf
-bash: cd: httpd.conf: Not a directory
[alienor@web conf]$ cd httpd.conf
-bash: cd: httpd.conf: Not a directory
[alienor@web conf]$ cd /etc/httpd/conf/httpd.conf
-bash: cd: /etc/httpd/conf/httpd.conf: Not a directory
[alienor@web conf]$ cat httpd.conf
#
# This is the main Apache HTTP server configuration file.  It contains the
# configuration directives that give the server its instructions.
# See <URL:http://httpd.apache.org/docs/2.4/> for detailed information.
# In particular, see
# <URL:http://httpd.apache.org/docs/2.4/mod/directives.html>
# for a discussion of each configuration directive.
#
# See the httpd.conf(5) man page for more information on this configuration,
# and httpd.service(8) on using and configuring the httpd service.
#
# Do NOT simply read the instructions in here without understanding
# what they do.  They're here only as hints or reminders.  If you are unsure
# consult the online docs. You have bee




🌞 Créer un VirtualHost qui accueillera NextCloud


[alienor@web conf]$ cat virtualhost
<VirtualHost *:80>
  # on précise ici le dossier qui contiendra le site : la racine Web
  DocumentRoot /var/www/nextcloud/html/

  # ici le nom qui sera utilisé pour accéder à l'application
  ServerName  web.tp5.linux

  <Directory /var/www/nextcloud/html/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>

[alienor@web conf]$




🌞 Configurer la racine web



🌞 Configurer PHP

[alienor@web ~]$ timedatectl
               Local time: Thu 2021-12-02 16:30:08 CET
           Universal time: Thu 2021-12-02 15:30:08 UTC
                 RTC time: Thu 2021-12-02 15:29:55
                Time zone: Europe/Paris (CET, +0100)
System clock synchronized: yes
              NTP service: active
          RTC in local TZ: no
[alienor@web ~]$






🌞 Récupérer Nextcloud


[alienor@web php74]$ sudo nano php.ini
[alienor@web php74]$ cd
[alienor@web ~]$ curl -SLO https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
 62  148M   62 92.9M    0     0  8814k      0  0:00:17  0:00:10  0:00:07  9.9M^C
[alienor@web ~]$



[alienor@web ~]$ ls
default_server  enpp  nextcloud-21.0.1.zip  y  y.pub






🌞 Ranger la chambre




🌞 Modifiez le fichier hosts de votre PC




🌞 Tester l'accès à NextCloud et finaliser son install'


