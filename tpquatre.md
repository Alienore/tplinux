
🌞 Choisissez et définissez une IP à la VM

[alienor@enp0s8 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:3b:09:f6 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 81123sec preferred_lft 81123sec
    inet6 fe80::a00:27ff:fe3b:9f6/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:14:91:6e brd ff:ff:ff:ff:ff:ff
    inet 10.200.1.2/24 brd 10.200.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe14:916e/64 scope link
       valid_lft forever preferred_lft forever

🌞 Vous me prouverez que service ssh actif:


[alienor@enp0s8 ~]$ systemctl status
● enp0s8
    State: running
     Jobs: 0 queued
   Failed: 0 units
    Since: Wed 2021-11-24 11:33:45 CET; 15h ago
   CGroup: /
           ├─user.slice
           │ └─user-1000.slice
           │   ├─user@1000.service
           │   │ └─init.scope
           │   │   ├─1431 /usr/lib/systemd/systemd --user
           │   │   └─1435 (sd-pam)



un échange de clés

PS C:\Users\alien\.ssh> type .\id_rsa.pub
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCzy5DPKwKaD+qwKWc9ohmAXAk2t3CrJf+j+2WbbIpMMGBSKAmgRWCvX5mEgMd2mGeD14AAQJapqIwVbpFUPMnGkZ8AGfd6cIhdWoTKCTdJHeYtc2R5XnwpZvi7lzRgrFkKcxc3b2jKy3m24+I6IvyelQ8XFw0E7JQdSnC1DT42VDuli1QKB6k/qyK1cYUu1I+JJUuBuVr4+XwZZmQVdDjsdZidio467pKlMMpMbLS5yCDL7pYORwKS6lyZRh6QLaqMQTkALF+gjVP9wa8XE7nO0zKvK/m1aZ50aUHJaMNo+XtqwuQonz+ahfwOLlnB20oslnSPWUkKbreS6BrkjWYTr1Fp2W7DRoYDYESmsGQKgAbNtw7/t6ocyghx4izh27upnev572xP5QfEzTnlGUvb52tcLr/aB7dBbQkGC0oZzry/p02lmPGmTFvbkutq2p1s5d3YLLMW2cVapJTgIyePsaKX6Cs+c/sAWTl2RfgvktG6UWmkafww7PEb27c2uVlkKz3WY/bSI+TW7bLulRKhLqcGZbiGvAgNfGzaqZj9Su0PAKOSkh10WwUJOPF0/FZZ0okVzVu/EqbwlmcfLMRKbxeMox1+nKeb9kVOSCmibMp3df1jJcNzxT0nXbIeGIWScd6JifNcUpbcPmbsL8W6f4e1lJZWg0GnxacjxxuzpQ== alien@LAPTOP-F1TNBS44

alien@LAPTOP-F1TNBS44 MINGW64 ~
$ ssh alienor@10.200.1.2
alienor@10.200.1.2's password:
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Thu Nov 25 02:48:14 2021 from 10.200.1.1
[alienor@node1 ~]$ ssh-copy-id alienor@10.200.1.2
/usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: "/home/alienor/.ssh/id_rsa.pub"
The authenticity of host '10.200.1.2 (10.200.1.2)' can't be established.
ECDSA key fingerprint is SHA256:I0E99wkgNCOOr6eqS6OcpPTkcRpBraIi8yLs/W1HN4Y.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
/usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys
alienor@10.200.1.2's password:

Number of key(s) added: 1

Now try logging into the machine, with:   "ssh 'alienor@10.200.1.2'"
and check to make sure that 

[alienor@node1 ~]$ ssh alienor@10.200.1.2
Load key "/home/alienor/.ssh/id_rsa": invalid format
alienor@10.200.1.2's password:
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Thu Nov 25 04:21:03 2021 from 10.200.1.1





[alienor@node1 ~]$ cat .ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCkqjTFafZZYyLGa6ug/WQTQRFVMa6ppngWsQ9hzG2eRRltxmxChEYw9JKx47g+M6xQOlNlC3RyyNqqZLy1kpuWzSDrRmcZPWvuFqLjTZa/lk+T6RWyxpN4sA+ApOrq36iAxv1kwbOWio9WVlaRnByy27mfgIsmhqcTsY80MdBVQnuFHZGfolERqjCz2jOa3tpXsMSWMU43hWZf4K24iWJhkUh2yUif8H0PefGQ830QuiQzFUXLugn5Ac3b4li8uMEEVMnk6cr4SMg3MGzdDhOEncvNX6K6D+kNMonZDrE9CUOWUZzN86UgFVMrzqyJve2dvMMMgRl3YbBp9Rj5nYEcXvUpTmNCx1FHNlxfUTNTyQtZ5ZrixHVh0GdAzSaM2P5hjWpP/neIAiGL8E3z1s9gbovziIErVGjmxMZROfL9Y6SH6lbfoQ4gikJGTnuZASCbxv276DYHJdswK+YQTHQ7sA9nXdA5/JvMvO3I3wcZRRBiuUrZYMWt1QlAkQxgYu5HqH4/CmlUvJe2/Nwof5+nirj4rEL98FvmVbVqttm/BDPKEifHSsptmTdGF3Ls4NgDHHDOrv/1ZEnScaT5Vi2sEi2Sw7bQZ0NQZnHJ6SG1xcvu/L68x287AHcrXt/R55A0mvgKl5PY8UzWg5dl34q5S98Z8SXvLPTl2gcEtuKQAw== alienor@enp0s8

$ ssh-copy-id alienor@10.200.1.2
/usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: "/c/Users/alien/.ssh/id_rsa.pub"
/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
/usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys
alienor@10.200.1.2's password:

Number of key(s) added: 1

Now try logging into the machine, with:   "ssh 'alienor@10.200.1.2'"
and check to make sure that only the key(s) you wanted were added.


alien@LAPTOP-F1TNBS44 MINGW64 ~
$ ssh alienor@10.200.1.2
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Thu Nov 25 04:46:50 2021 from 10.200.1.2




🌞 Prouvez que vous avez un accès internet


[alienor@enp0s8 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=30.7 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=30.4 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=113 time=30.2 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=113 time=31.2 ms
^C
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3014ms
rtt min/avg/max/mdev = 30.157/30.610/31.195/0.449 ms
[alienor@enp0s8 ~]$ ping 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=54 time=28.1 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=54 time=29.8 ms
64 bytes from 1.1.1.1: icmp_seq=3 ttl=54 time=28.2 ms
^C
--- 1.1.1.1 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2008ms
rtt min/avg/max/mdev = 28.111/28.686/29.757/0.782 ms
[alienor@enp0s8 ~]$



🌞 Prouvez que vous avez de la résolution de nom
Un petit vers un nom de domaine, celui que vous voulez :)ping

[alienor@node1 ~]$ ping ynov.com
PING ynov.com (92.243.16.143) 56(84) bytes of data.
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=1 ttl=51 time=26.1 ms
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=2 ttl=51 time=28.6 ms
^C
--- ynov.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1005ms
rtt min/avg/max/mdev = 26.134/27.377/28.621/1.254 ms



🌞 Définissez node1.tp4.linux comme nom à la machine

alienor@enp0s8 ~]$ hostname
node1.tp4.linux



🌞 Installez NGINX en vous référant à des docs online

[alienor@node1 ~]$ sudo dnf install nginx
Last metadata expiration check: 23:44:10 ago on Wed 24 Nov 2021 11:13:26 AM CET.
Dependencies resolved.

[alienor@node1 ~]$ sudo systemctl start nginx

[alienor@node1 ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor pres>
   Active: active (running) since Thu 2021-11-25 11:00:13 CET; 13min ago
  Process: 6369 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
  Process: 6368 ExecStartPre=/usr/sbin/nginx -t 



🌞 Analysez le service NGINX



[alienor@node1 ~]$ ps -ef | grep nginx
root        6518       1  0 11:38 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx       6519    6518  0 11:38 ?        00:00:00 nginx: worker process
alienor     6545    6478  0 11:42 pts/0    00:00:00 grep --color=auto nginx
[alienor@node1 ~]$


[alienor@node1 ~]$ sudo ss -ltnp | grep nginx
LISTEN 0      128          0.0.0.0:80        0.0.0.0:*    users:(("nginx",pid=6519,fd=8),("nginx",pid=6518,fd=8))
LISTEN 0      128             [::]:80           [::]:*    users:(("nginx",pid=6519,fd=9),("nginx",pid=6518,fd=9))





[alienor@node1 ~]$ sudo cat /etc/nginx/nginx.conf | grep nginx
 root         /usr/share/nginx/html;




🌞 Configurez le firewall pour autoriser le trafic vers le service NGINX 


[alienor@node1 ~]$ sudo firewall-cmd --permanent --add-service=http
success


🌞 Tester le bon fonctionnement du service


alien@LAPTOP-F1TNBS44 MINGW64 ~
$ http://10.200.1.2:22


$ curl nginx
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   228  100   228    0     0   1299      0 --:--:-- --:--:-- --:--:--  1310<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>301 Moved Permanently</title>
</head><body>
<h1>Moved Permanently</h1>
<p>The document has moved <a href="http://www.ynov.com/">here</a>.</p>
</body></html>



🌞 Changer le port d'écoute


[alienor@node1 ~]$ listen 8080 default_server;




🌞 Changer l'utilisateur qui lance le service


$ ssh toto@10.200.1.2
toto@10.200.1.2's password:
Activate the web console with: systemctl enable --now cockpit.socket

Last failed login: Thu Nov 25 12:59:42 CET 2021 from 10.200.1.1 on ssh:notty
There was 1 failed login attempt since the last successful login.


[toto@node1 ~]$ cd ..
[toto@node1 home]$ ls
alienor  toot  toto  web
[toto@node1 home]$

sudo nano /etc/passwd
toto:x:1002:1002::/home/web:/bin/bash






🌞 Changer l'emplacement de la racine Web

[alienor@node1 ~]$ cd /var/
[alienor@node1 var]$ sudo mkdir /www/
[alienor@node1 var]$ cd /www/
[alienor@node1 www]$ sudo mkdir /super_site_web
[alienor@node1 www]$


[alienor@node1 super_site_web]$ sudo nano index.html
[alienor@node1 super_site_web]$ ls
index.html  mkdir
[alienor@node1 super_site_web]$ cat index.html
<h1>toto</h1>




